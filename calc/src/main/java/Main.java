import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        try {
            Scanner in = new Scanner(System.in);
            System.out.print("Enter first value: ");
            double a = in.nextDouble();
            System.out.print("Enter second value: ");
            double b = in.nextDouble();
            System.out.print("Enter +,-,*,/: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            String c = reader.readLine();



            if (c.equals("+")) {
                Calc calc = new Calc(a, b, c);
                System.out.println(calc.addition());
            }
            else if (c.equals("-")) {
                Calc calc = new Calc(a, b, c);
                System.out.println(calc.subtraction());
            }
            else if (c.equals("*")) {
                Calc calc = new Calc(a, b, c);
                System.out.println(calc.multiplication());
            }
            else if (c.equals("/")) {
                Calc calc = new Calc(a, b, c);
                System.out.println(calc.addition());
            }
            else
                System.out.println("ERROR");

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
